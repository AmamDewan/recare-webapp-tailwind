import Vue from 'vue'
import * as AmplifyModules from '@aws-amplify/core'
import Auth from '@aws-amplify/auth'
import { AmplifyPlugin } from 'aws-amplify-vue'
Auth.configure({
  region: process.env.awsRegion,
  userPoolId: process.env.cognitoUserPoolId,
  userPoolWebClientId: process.env.cognitoUserPoolWebClientId,
  oauth: {
    // domain: process.env.cognitoDomain,
    scope: ['email', 'openid', 'profile', 'aws.cognito.signin.user.admin'],
    redirectSignIn: window.location.origin,
    redirectSignOut: window.location.origin,
    responseType: 'token', // or 'token', note that REFRESH token will only be generated when the responseType is code
  },
  authenticationFlowType: 'USER_SRP_AUTH',
})

Vue.use(AmplifyPlugin, AmplifyModules)

export default (_, inject) => {
  inject('Amplify', AmplifyModules)
}
