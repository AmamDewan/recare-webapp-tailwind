let http = null

class API {
  constructor(options) {
    http = options.http
  }

  setToken(token, type = 'Bearer', scopes = 'common') {
    http.setToken(token, type, scopes)
  }

  clearToken() {
    this.setToken(false)
  }

  get(resource, params = {}) {
    return http.$get(resource, { params })
  }

  put(resource, data = {}, params = {}) {
    return http.$put(resource, data, {
      params,
    })
  }

  post(resource, data = {}, params = {}) {
    return http.$post(resource, data, {
      params,
    })
  }
  delete(resource, data = {}, params = {}) {
    return http.$request(resource, {
      data,
      method: 'delete',
    })
  }
}
export default API
