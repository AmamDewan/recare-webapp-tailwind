let AmplifyAuth = null
let store = null

class Auth {
  constructor(option) {
    AmplifyAuth = option.amplifyAuth
    store = option.store
  }

  isAuthenticated() {
    const session = this.getSession()
    if (!session) return false
    const accessToken = session.accessToken
    if (!accessToken) return false
    const hasToken = accessToken.jwtToken
    const isActive = new Date(accessToken.payload.exp * 1000) > new Date()
    const isMe =
      accessToken.payload.username === store.getters['auth/getUser'].username
    return hasToken && isActive && isMe
  }

  fetchUser() {
    return AmplifyAuth.currentAuthenticatedUser({ bypassCache: true }).then(
      (user) => {
        this.setUser(user)
        return user
      }
    )
  }

  getUser() {
    return store.getters['auth/getUser']
  }

  setUser(user = {}) {
    store.commit('auth/setUser', {
      ...{ username: user.username },
      ...user.attributes,
    })
  }

  fetchSession(options = { cache: true }) {
    return AmplifyAuth.currentSession().then((session) => {
      return AmplifyAuth.currentUserPoolUser({
        bypassCache: !options.cache,
      }).then((user) => {
        this.setUser(user)
        this.setSession(session)
        store.$api.setToken(this.getIdToken())
        return session
      })
    })
  }

  getSession() {
    return store.getters['auth/getSession']
  }

  setSession(session = {}) {
    store.commit('auth/setSession', session)
  }

  getAccessToken() {
    return this.getSession().getAccessToken().getJwtToken()
  }

  getIdToken() {
    return this.getSession().getIdToken().getJwtToken()
  }

  getUserGroups() {
    const userGroups = this.getSession().getIdToken().payload['cognito:groups']
    return userGroups || []
  }

  register(credentials) {
    return AmplifyAuth.signUp({
      username: credentials.username,
      password: credentials.password,
      attributes: credentials.attributes,
    }).then((user) => {
      this.setUser(user)
      return user
    })
  }

  confirmRegistration(data) {
    return AmplifyAuth.confirmSignUp(data.username, data.code)
  }

  resendConfirmation(data) {
    return AmplifyAuth.resendSignUp(data.username)
  }

  forgotPassword(data) {
    return AmplifyAuth.forgotPassword(data.username)
  }

  changePassword(data) {
    return AmplifyAuth.forgotPasswordSubmit(
      data.username,
      data.code,
      data.password
    )
  }

  login(credentials) {
    return AmplifyAuth.signIn({
      username: credentials.username,
      password: credentials.password,
    }).then((user) => {
      this.setSession(user.signInUserSession)
      this.setUser(user)
      store.$api.setToken(this.getIdToken())
      return user
    })
  }

  deleteAllCookies() {
    const cookies = document.cookie.split('; ')
    for (let c = 0; c < cookies.length; c++) {
      const d = window.location.hostname.split('.')
      while (d.length > 0) {
        const cookieBase =
          encodeURIComponent(cookies[c].split(';')[0].split('=')[0]) +
          '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' +
          d.join('.') +
          ' ;path='
        const p = location.pathname.split('/')
        document.cookie = cookieBase + '/'
        while (p.length > 0) {
          document.cookie = cookieBase + p.join('/')
          p.pop()
        }
        d.shift()
      }
    }
  }

  logout() {
    if (!this.isAuthenticated()) {
      throw new Error('User not logged in.')
    }

    return AmplifyAuth.signOut().then((res) => {
      this.setUser()
      this.setSession()
      store.$router.push('/')
      return res
    })
  }

  async updateUser(data) {
    console.log(data)
    const user = await this.fetchUser()
    console.log(user)
    return AmplifyAuth.updateUserAttributes(user, data)
  }

  federatedLogin(options = {}) {
    return AmplifyAuth.federatedSignIn({
      provider: options.provider || 'COGNITO',
    })
  }

  async updatePassword(credentials) {
    const currentUser = await AmplifyAuth.currentAuthenticatedUser()
    await AmplifyAuth.changePassword(
      currentUser,
      credentials.oldPassword,
      credentials.newPassword
    )
  }
}

export default Auth
